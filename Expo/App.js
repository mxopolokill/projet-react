import * as React from 'react';
import { Text } from 'react-native';
import ExpensesTabNavigation from './Navigation/Navigation';
import AuthentificationStackNavigation from './Navigation/AuthentificationNavigation'
import AsyncStorage from '@react-native-async-storage/async-storage';
import userStore from './stores/user';


const App = () => {
  const user = userStore()

  // On récupère l'id et le token de l'utilisateur stocké dans l'AsyncStorage de React Native
  // Grâce au store (zustand) on met à jour l'état global de l'utilisateur pour accéder à son token et son id pour l'ensemble des composants utilisés
  React.useEffect(() => {
    async function getUser() {
      const userLocal = {
        id: await AsyncStorage.getItem('id'),
        token: await AsyncStorage.getItem('token')
      }

      user.setUser(userLocal)
      user.setLoaded()
    }

    getUser()
  }, [])

  // On retourne la navigation ExpensesTabNavigation si l'utilisateur à un token, sinon il reste sur la navigation AuthentificationStackNavigation
  function returnNavigation() {
    if (user.token) return <ExpensesTabNavigation />

    return <AuthentificationStackNavigation />
  }

  return (
    <>
      {
        !user.loaded ? <Text>Loading</Text> : returnNavigation()
      }
    </>
  )
};

export default App;
