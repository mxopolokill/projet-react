import * as React from 'react';
import {
    Image,
    StyleSheet,
} from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Search from '../Components/Search'
import AddExpense from '../Components/AddExpense'
import ExpenseDetails from '../Components/ExpenseDetail';
import Stats from '../Components/Stats'
import Profil from '../Components/Profil'
import { FontAwesome } from '@expo/vector-icons';


const ExpensivesStack = createNativeStackNavigator();

function ExpensesStackNavigation() {
    return (
        <ExpensivesStack.Navigator>
            <ExpensivesStack.Screen
                name="Search"
                component={Search}
            />
            <ExpensivesStack.Screen
                name="ExpenseDetail"
                component={ExpenseDetails}
            />
        </ExpensivesStack.Navigator>
    );
};

const FormStack = createNativeStackNavigator();

function FormStackNavigation() {
    return (
        <FormStack.Navigator>
            <FormStack.Screen name="AddExpense" component={AddExpense} />
        </FormStack.Navigator>
    );
}

const StatStack = createNativeStackNavigator();

function StatStackNavigation() {
    return (
        <StatStack.Navigator>
            <StatStack.Screen name="Stats" component={Stats} />
        </StatStack.Navigator>
    );
}

const ProfilStack = createNativeStackNavigator();

function ProfilStackNavigation() {
    return (
        <ProfilStack.Navigator>
            <ProfilStack.Screen name="Profil" component={Profil} />
        </ProfilStack.Navigator>
    );
}

const Tab = createBottomTabNavigator();

export default function ExpensesTabNavigation() {
    return (
        <NavigationContainer>
            <Tab.Navigator
                screenOptions={{ headerShown: false }}>
                <Tab.Screen
                    name="Mes dépenses"
                    component={ExpensesStackNavigation}
                    options={{
                        tabBarIcon: ({ }) => (
                            <FontAwesome name="credit-card" size={24} color="black" />
                        ),
                    }}
                />
                <Tab.Screen
                    name="Ajouter une dépense"
                    component={FormStackNavigation}
                    options={{
                        tabBarIcon: ({ }) => (
                            <FontAwesome name="plus-square-o" size={24} color="black" />
                        ),
                    }}
                />
                <Tab.Screen
                    name="Mes statistics"
                    component={StatStackNavigation}
                    options={{
                        tabBarIcon: ({ }) => (
                            <FontAwesome name="area-chart" size={24} color="black" />
                        ),
                    }}
                />
                <Tab.Screen
                    name="Mon profil"
                    component={ProfilStackNavigation}
                    options={{
                        tabBarIcon: ({ }) => (
                            <FontAwesome name="user" size={24} color="black" />
                        ),
                    }}
                />
            </Tab.Navigator>
        </NavigationContainer>
    );
};

const styles = StyleSheet.create({
    icon: {
        width: 30,
        height: 30
    }
})