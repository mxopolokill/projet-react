import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Login from '../Components/Login'
import SignUp from '../Components/SignUp'

const AuthentificationStack = createNativeStackNavigator();

export default function AuthentificationStackNavigation() {
    return (
        <NavigationContainer>
            <AuthentificationStack.Navigator>
                <AuthentificationStack.Screen
                    name="Login"
                    component={Login}
                />
                <AuthentificationStack.Screen
                    name="SignUp"
                    component={SignUp}
                />
            </AuthentificationStack.Navigator>
        </NavigationContainer>
    );
};