// Utilisation de la librairie zustand pour modifier l'état global de l'utilisateur et ainsi récupérer son token et son id pour l'ensemble des composants

import create from 'zustand'

const userStore = create(set => ({
  token: undefined,
  id: undefined,
  loaded: false,
  setUser: (user) => set({ token: user.token, id: user.id }),
  setLoaded: () => set({ loaded: true })
}))

export default userStore;