// Mappage des items rendus par le composant Search.js
import React from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import numeral from 'numeral'

export default function Expenseprops(props) {
  return (
    // Au click déclenchement de la function de détails des dépenses défini dans le composant search.js avec l'id du l'item
    <TouchableOpacity onPress={() => props.displayDetailForExpense(props.item.id)} style={styles.main_container}>
      <Image
        style={styles.image}
        source={{
          uri: props.item.photo,
        }}
      />
      <View style={styles.content_container}>
        <View style={styles.header_container}>
          <Text style={styles.title_text}>{props.item.title}</Text>
          <Text style={styles.amount_text}>{numeral(props.item.amount).format('0,0[.]00')}€</Text>
        </View>
        <View style={styles.description_container}>
          <Text style={styles.description_text} numberOfLines={6}>{props.item.description}</Text>
          {/* La propriété numberOfLines permet de couper un texte si celui-ci est trop long, il suffit de définir un nombre maximum de ligne */}
        </View>
        <View style={styles.date_container}>
          <Text style={styles.date_text}>Date : {props.item.CreatedAt}</Text>
        </View>
        <View style={styles.date_container}>
          <Text style={styles.date_text}>Mode de paiement : {props.item.paymentMethod}</Text>
        </View>
        <View style={styles.date_container}>
          {/* Short Circuit nécessaire pour l'affichage des propriétés de niveau 2 */}
          {props.item.category &&
            <Text style={styles.date_text}>Catégorie : {props.item.category.name}</Text>
          }
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  main_container: {
    height: 190,
    flexDirection: 'row'
  },
  image: {
    width: 120,
    height: 180,
    margin: 5,
    backgroundColor: 'gray'
  },
  content_container: {
    flex: 1,
    margin: 5
  },
  header_container: {
    flex: 3,
    flexDirection: 'row'
  },
  title_text: {
    fontWeight: 'bold',
    fontSize: 20,
    flex: 1,
    flexWrap: 'wrap',
    paddingRight: 5
  },
  amount_text: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#666666'
  },
  description_container: {
    flex: 7
  },
  description_text: {
    fontStyle: 'italic',
    color: '#666666'
  },
  date_container: {
    flex: 1
  },
  date_text: {
    textAlign: 'right',
    fontSize: 14
  }
});
