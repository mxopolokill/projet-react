import React, { useState, useEffect } from 'react';
import { Button, Text, View } from 'react-native';
import axios from "axios";
import userStore from "../stores/user";
import AsyncStorage from "@react-native-async-storage/async-storage";
import { urlApi } from "../Api/urlApi";


export default function Home({ navigation }) {
    const user = userStore()

    const [data, setData] = useState([]);

    // Récupération des datas de l'utilisateur
    function getUserDetails() {
        try {
            console.log(urlApi + "/users/" + user.id);
            axios.get(urlApi + "/users/" + user.id).then((res) => {
                setData(res.data);
            });
        } catch (error) {
            console.error(error);
        }
    }

    // On supprime le token et l'id de l'utilisateur en question dans l'AsyncStorage de React Native
    // On recharge la page pour retrouver la navigation pour un user non connecté
    async function deconnexionUser() {
        try {
            await AsyncStorage.removeItem('token');
            await AsyncStorage.removeItem('id');
            window.location.reload()
        } catch (error) {
            console.log(error)
        }
    }

    useEffect(() => {
        getUserDetails()
    }, []);

    return (
        <View style={{ flex: 1 }}>
            <Text>Pseudo : {data.pseudo}</Text>
            <Text>Email : {data.email}</Text>
            <Button title="Deconnexion" onPress={() => deconnexionUser()} />
        </View>
    );
}