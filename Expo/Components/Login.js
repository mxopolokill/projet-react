import React from "react";
import {
    View,
    Text,
    Button,
    TextInput,
    StyleSheet,
} from "react-native";
import { Formik } from "formik";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as yup from 'yup'
import userStore from "../stores/user";
import { urlApi } from "../Api/urlApi";


export default function Login({ navigation }) {
    const user = userStore()

    // Schéma de validation du formulaire avec le composant yup
    const loginValidationSchema = yup.object().shape({
        email: yup
            .string()
            .email("Please enter valid email")
            .required("Email Address is Required"),
        password: yup
            .string()
            .min(4, ({ min }) => `Password must be at least ${min} characters`)
            .required("Password is required"),
    });

    // Requete API Post => création d'un token pour l'utilisateur inscrit
    function getTokenUser(values) {
        axios
            // Ajout des datas nécessaires
            .post(urlApi + "/login_check", {
                email: values.email,
                password: values.password,
            })
            // Enregistrement du token et de l'id de l'utilisateur dans l'AsyncStorage de React Native 
            // MAJ des données du state global de l'utilisateur
            // Rechargement de la page pour passer de la navigation d'un user non connectée à connectée
            .then(function (response) {
                AsyncStorage.setItem("token", response.data.token);
                AsyncStorage.setItem("id", response.data.data.id);
                user.setUser(response.data);
                window.location.reload()
            })
            .catch(function (error) {
                console.log(error)
            });
    }

    return (
        <View style={styles.loginContainer}>
            <Text>Connexion</Text>
            <Formik
                validationSchema={loginValidationSchema}
                initialValues={{ email: "", password: "" }}
                onSubmit={(values) => getTokenUser(values)}
            >
                {({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    errors,
                    isValid,
                    values,
                    touched,
                }) => (
                    <>
                        <TextInput
                            name="email"
                            placeholder="Email"
                            style={styles.textInput}
                            onChangeText={handleChange("email")}
                            onBlur={handleBlur("email")}
                            value={values.email}
                            keyboardType="email-address"
                        />
                        {(errors.email && touched.email) &&
                            <Text style={{ fontSize: 10, color: "red" }}>{errors.email}</Text>
                        }
                        <TextInput
                            name="password"
                            placeholder="Mot de passe"
                            style={styles.textInput}
                            onChangeText={handleChange("password")}
                            onBlur={handleBlur("password")}
                            value={values.password}
                            secureTextEntry
                        />
                        {(errors.password && touched.password) &&
                            <Text style={{ fontSize: 10, color: "red" }}>
                                {errors.password}
                            </Text>
                        }
                        <Button
                            onPress={handleSubmit}
                            title="Connexion"
                            disabled={!isValid} />

                        <Button
                            onPress={() => navigation.navigate("SignUp")}
                            title="Inscription"
                        />
                    </>
                )}
            </Formik>
        </View>
    );
}

const styles = StyleSheet.create({
    loginContainer: {
        width: "80%",
        alignItems: "center",
        backgroundColor: "white",
        padding: 10,
        elevation: 10,
        backgroundColor: "#e6e6e6",
    },
    textInput: {
        height: 40,
        width: "100%",
        margin: 10,
        paddingLeft: 10,
        backgroundColor: "white",
        borderColor: "gray",
        borderWidth: StyleSheet.hairlineWidth,
        borderRadius: 10,
    },
});
