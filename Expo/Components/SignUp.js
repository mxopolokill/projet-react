import React from 'react'
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  StatusBar,
  Button,
} from 'react-native'
import axios from "axios";
import { Formik, Field } from 'formik'
import * as yup from 'yup'
import CustomInput from './CustomInput'
import { urlApi } from "../Api/urlApi";


export default function SignUp({ navigation }) {

  // Schéma de validation du formulaire avec le composant yup
  const signUpValidationSchema = yup.object().shape({
    pseudo: yup
      .string()
      .required('Pseudo is required'),
    email: yup
      .string()
      .email("Please enter valid email")
      .required('Email is required'),
    password: yup
      .string()
      // .matches(/\w*[a-z]\w*/,  "Password must have a small letter")
      // .matches(/\w*[A-Z]\w*/,  "Password must have a capital letter")
      // .matches(/\d/, "Password must have a number")
      // .matches(/[!@#$%^&*()\-_"=+{}; :,<.>]/, "Password must have a special character")
      .min(4, ({ min }) => `Password must be at least ${min} characters`)
      .required('Password is required'),
    confirmPassword: yup
      .string()
      .oneOf([yup.ref('password')], 'Passwords do not match')
      .required('Confirm password is required'),
  })

  //requete API Post => inscription de l'utilisateur => ajout dans la BDD
  function signUpUser(values) {
    axios
      .post(urlApi + "/users", {
        pseudo: values.pseudo,
        email: values.email,
        password: values.password,
      })
      .then(() => navigation.navigate('Login'))
      .catch(function (error) {
        console.log(error);
      });
  }

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={styles.container}>
        <View style={styles.signupContainer}>
          <Text>Inscription</Text>
          <Formik
            validationSchema={signUpValidationSchema}
            initialValues={{
              pseudo: '',
              email: '',
              password: '',
              confirmPassword: '',
            }}
            onSubmit={values => signUpUser(values)}
          >
            {({ handleSubmit, isValid }) => (
              <>
                <Field
                  component={CustomInput}
                  name="pseudo"
                  placeholder="Pseudo"
                />
                <Field
                  component={CustomInput}
                  name="email"
                  placeholder="Email"
                  keyboardType="email-address"
                />
                <Field
                  component={CustomInput}
                  name="password"
                  placeholder="Mot de passe"
                  secureTextEntry
                />
                <Field
                  component={CustomInput}
                  name="confirmPassword"
                  placeholder="Répéter le mot de passe"
                  secureTextEntry
                />

                <Button
                  onPress={handleSubmit}
                  title="Inscription"
                  disabled={!isValid}
                />
              </>
            )}
          </Formik>
        </View>
      </SafeAreaView>
    </>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  signupContainer: {
    width: '80%',
    alignItems: 'center',
    backgroundColor: 'white',
    padding: 10,
    elevation: 10,
    backgroundColor: '#e6e6e6'
  },
})