import React, { useEffect, useState } from "react";
import {
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    ActivityIndicator,
} from "react-native";
import axios from "axios";
import numeral from 'numeral';
import { urlApi } from "../Api/urlApi";

export default function ExpenseDetails(props) {
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);

    function getExpensesDetails(id) {
        try {
            axios.get(urlApi + "/expenses/" + id).then((res) => {
                setData(res.data);
            });
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    useEffect(() => {
        getExpensesDetails(props.route.params.id);
    }, []);

    return (
        <View style={styles.main_container}>
            {isLoading ? (
                <ActivityIndicator />
            ) : (
                <ScrollView style={styles.scrollview_container}>
                    {data.photo && <Image
                        style={styles.image}
                        source={{ uri: data.photo }}
                    />}
                    <Text style={styles.title_text}>{data.title}</Text>
                    <Text style={styles.description_text}>{data.description}</Text>
                    <Text style={styles.default_text}>Date : {data.CreatedAt}</Text>
                    <Text style={styles.default_text}>Montant : {numeral(data.amount).format('0,0[.]00')}€</Text>
                    <Text style={styles.default_text}>Mode de paiement: {data.paymentMethod}</Text>
                    <Text style={styles.default_text}>Catégorie: {data.category && data.category.name}</Text>
                </ScrollView>
            )}
        </View>
    );
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    scrollview_container: {
        flex: 1
    },
    image: {
        height: 169,
        margin: 5
    },
    title_text: {
        fontWeight: 'bold',
        fontSize: 35,
        flex: 1,
        flexWrap: 'wrap',
        marginLeft: 5,
        marginRight: 5,
        marginTop: 10,
        marginBottom: 10,
        color: '#000000',
        textAlign: 'center'
    },
    description_text: {
        fontStyle: 'italic',
        color: '#666666',
        margin: 5,
        marginBottom: 15
    },
    default_text: {
        marginLeft: 5,
        marginRight: 5,
        marginTop: 5,
    }
})
