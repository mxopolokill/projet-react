import React, { useEffect, useState } from "react";
import {
    StyleSheet,
    View,
    Button,
    TextInput,
    FlatList,
    ActivityIndicator,
} from "react-native";
import ExpenseItem from "./ExpenseItem";
import axios from 'axios';
import { urlApi } from "../Api/urlApi";
import userStore from "../stores/user";
import { Field, Form, Formik } from 'formik';

export default function Search(props) {

    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [category, setCategory] = useState("");
    const [searchItem, setSearchItem] = useState("");
    const [list, setList] = useState("");
    const [month, setMonth] = useState([]);
    const [year, setYear] = useState("");

    const user = userStore()

    // Boucle permettant l'affichage des mois de l'année (1 à 12) au chargement de la page
    function getMonth() {
        let listMonth = [];
        for (let i = 1; i < 13; i++) {
            let list = <option value={i} key={i}>{i}</option>
            listMonth.push(list)
        }
        setMonth(listMonth);
    }

    // Boucle permettant l'affichage des deux dernières années au chargement de la page
    function getYear() {
        let listYear = [];
        for (let i = 2021; i > 2019; i--) {
            let list = <option value={i} key={i}>{i}</option>
            listYear.push(list)
        }
        setYear(listYear);
    }

    // On récupères toutes les dépenses de l'utilisateur
    function getAllExpenses() {
        try {
            axios.get(urlApi + "/user/" + user.id)
                .then(res => {
                    setList(res.data);
                });
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    // On récupères toutes les catégories créent par l'administrateur
    function getCat() {
        try {
            axios.get(urlApi + "/categories")
                .then(res => {
                    const listItems = res.data['hydra:member'].map((category, i) =>
                        <option value={category.id} key={i}>{category.name}</option>)
                    setCategory(listItems);
                });
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    // On récupères toutes les dépenses de l'utilisateur par catégories
    function getExpensesByCat(catId) {
        if (catId == '' || catId == 'Catégories') {
            getExpensesLastMonth()
        }
        else {
            try {
                axios.get(urlApi + "/user/" + user.id + "/" + catId)
                    .then(res => {
                        setData(res.data);
                    });
            } catch (error) {
                console.error(error);
            } finally {
                setLoading(false);
            }
        }
    }

    // On récupères toutes les dépenses de l'utilisateur par mois et année
    function getExpensesByMonth(month, year) {
        if (month == '' || month == 'Mois' || year == "" || year == 'Année') {
            getExpensesLastMonth()
        }
        try {
            axios.get(urlApi + "/user/" + user.id + "/" + year + "/" + month)
                .then(res => {
                    setData(res.data);
                });
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    // On récupères toutes les dépenses de l'utilisateur des 30 derniers jours
    function getExpensesLastMonth() {
        try {
            axios.get(urlApi + "/user/" + user.id + "/lastmonth")
                .then(res => {
                    setData(res.data);
                });
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }

    // On crée une fonction permettant d'envoyer l'id de la dépense au composant ExpenseDetail.js
    function displayDetailForExpense(idExpense) {
        props.navigation.navigate("ExpenseDetail", { id: idExpense })
    }

    useEffect(() => {
        getAllExpenses();
        getCat();
        getMonth();
        getYear();
        getExpensesLastMonth();
    }, []);

    useEffect(() => {
        // On commence à rechercher si le mot utilisé dans la barre comportent au moins 2 caractères
        if (searchItem < 2 && searchItem !== 0) {
            getExpensesLastMonth();
        } else {
            const filterExpenses = list.filter((expense) => {
                // On transforme le mot recherché en miniscule pour avoir une recherche non sensible à la casse
                // On filtre uniquement sur le champ titre des dépenses
                return JSON.stringify(expense.title).toLocaleLowerCase().includes(searchItem.toLocaleLowerCase());
            })
            setData(filterExpenses)
        }
    }, [searchItem])

    // On récupère la valeur du mot entré dans la barre de recherche à chaque changement de caractères
    function handleSearch(e) {
        let value = e.target.value;
        setSearchItem(value);
    }

    return (
        <View style={styles.main_container}>
            <TextInput style={styles.text_input} placeholder="Titre de la recherche" onChange={handleSearch} />
            <Formik
                initialValues={{ category: '' }}
                onSubmit={(values) => getExpensesByCat(values.category)}
            >
                {({
                    handleSubmit,
                }) => (
                    <>
                        <Form>
                            <Field as="select" name="category">
                                <option key="0">Catégories</option>
                                {category}
                            </Field>
                            <Button title="Valider" onPress={handleSubmit} />
                        </Form>
                    </>
                )}
            </Formik>
            <Formik
                initialValues={{ month: '', year: '' }}
                onSubmit={(values) => getExpensesByMonth(values.month, values.year)}
            >
                {({
                    handleSubmit,
                }) => (
                    <>
                        <Form>
                            <Field as="select" name="month">
                                <option key="0">Mois</option>
                                {month}
                            </Field>
                            <Field as="select" name="year">
                                <option key="0">Année</option>
                                {year}
                            </Field>
                            <Button title="Valider" onPress={handleSubmit} />
                        </Form>
                    </>
                )}
            </Formik>
            {
                isLoading ? (
                    <ActivityIndicator />
                ) : (
                    <FlatList
                        data={data}
                        keyExtractor={({ id }, index) => id}
                        renderItem={({ item }) => <ExpenseItem item={item} displayDetailForExpense={displayDetailForExpense} />}
                    />
                )
            }
        </View >
    );
}

const styles = StyleSheet.create({
    main_container: {
        flex: 1
    },
    text_input: {
        marginLeft: 5,
    }
});
