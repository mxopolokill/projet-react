import React, { useState } from 'react';
import { Button, Text, TextInput, View, StyleSheet, StatusBar, TouchableOpacity, Image, FlatList } from 'react-native';
import { Formik } from 'formik';
import axios from "axios";
import * as Animatable from 'react-native-animatable';
import { urlApi } from "../Api/urlApi";
import userStore from "../stores/user";
//import galleryImage from '../assets/icons/gallery.png';



export default function Home({ navigation }) {
  const user = userStore()
  {/*const [selectedImage, setSelectedImage] = useState([]); */ }

  function postExpenses(values) {
    axios.post(

      urlApi + "/expenses",
      {
        title: values.title,
        description: values.description,
        amount: parseInt(values.amount),
        user: "/api/users/" + user.id,
        paymentMethod: values.payment_method,
        photo: values.photo
      }
    ).then(function () {
      navigation.navigate('Search')
      window.location.reload()
    }).catch(function (errors) {
      console.log(errors)
    })
  }

  {/*
          Récuperation image Gallerie avec permission       
          Probléme de stpckage en interne 
    let openImagePickerAsync = async () => {
    let permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      alert('Permission to access camera roll is required!');
      return;
    }

    let pickerResult = await ImagePicker.launchImageLibraryAsync();
    if (pickerResult.cancelled === true) {
      return;
    }

    setSelectedImage({ localUri: pickerResult.uri });
  };

  if (selectedImage !== null) {
    
    <>
        <Image source={{ uri: selectedImage.localUri }} style={styles.thumbnail} />
        </>
  }
*/}


  return (


    <View style={styles.container}>
      <StatusBar backgroundColor='#009387' barStyle="light-content" />

      <Formik
        initialValues={{ title: '', description: '', amount: '', payment_method: '', photo: '' }}
        onSubmit={(values) => postExpenses(values)}

      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (

          <>
            <View style={styles.header}><Text style={styles.text_header}>Ajouter une dépense</Text></View>
            <Animatable.View animation="fadeInUpBig" style={styles.footer}>

              <Text style={styles.text_footer}>Titre</Text>
              <TextInput
                placeholder="titre dépense"
                onChangeText={handleChange('title')}
                value={values.title}
              />

              <Text style={styles.text_footer}>description</Text>
              <TextInput
                placeholder="description"
                onChangeText={handleChange('description')}
                value={values.description}
              />

              <Text style={styles.text_footer}>Montant</Text>
              <TextInput
                placeholder="amount"
                onChangeText={handleChange("amount")}
                value={values.amount}
              />

              <Text style={styles.text_footer}>methode de paiement</Text>
              <TextInput
                placeholder="payment_method"
                onChangeText={handleChange('payment_method')}
                value={values.payment_method}
              />

              <Text style={styles.text_footer}>photo</Text>
              <TextInput
                placeholder="photo"
                onChangeText={handleChange('photo')}
                value={values.photo}
              />

              {/* 
                    bouton Récuperation image Gallerie                 
                  <View style={styles.container}>
                 
                   <View style={styles.buttonContainer}>

           
                   <View style={styles.container}>
                      <TouchableOpacity onPress={openImagePickerAsync} style={styles.button}  value={values.photo}>
                        <Text style={styles.buttonText}>Pick a photo</Text>
                      </TouchableOpacity>
                    </View>   
                   </View>
                 
                    </View> 
                */}

              <Button style={styles.button}
                onPress={handleSubmit}
                title="Ajouter" />

            </Animatable.View>
          </>
        )}

      </Formik>
    </View>

  );

}

const styles = StyleSheet.create({

  container: {
    flex: 1,
    backgroundColor: '#009387'
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50
  },

  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30
  },

  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30
  },

  text_footer: {
    color: '#000000',
    fontSize: 15
  },

  button: {
    width: '80%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },

});
